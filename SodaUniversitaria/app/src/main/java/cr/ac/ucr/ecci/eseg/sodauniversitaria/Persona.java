package cr.ac.ucr.ecci.eseg.sodauniversitaria;
        import android.os.Parcel;
        import android.os.Parcelable;
/**
 * Created by Christian on 2/26/2017.
 */
public class Persona implements Parcelable {
    private String identificacion;
    private String correo;
    private String nombre;
    private String celular;
    private String primerApellido;
    private String segundoApellido;

    public Persona(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido) {
        this.identificacion = identificacion;
        this.correo = correo;
        this.nombre = nombre;
        this.celular = celular;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
    }

    //Constructor vacío para pruebas
    public Persona() {
        this.identificacion = "1234";
        this.correo = "prueba@gmail.com";
        this.nombre = "sebastian";
        this.celular = "88929619";
        this.primerApellido = "vargas";
        this.segundoApellido = "soto";
    }

    protected Persona(Parcel in) {
        identificacion = in.readString();
        correo = in.readString();
        nombre = in.readString();
        primerApellido = in.readString();
        segundoApellido = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identificacion);
        dest.writeString(correo);
        dest.writeString(nombre);
        dest.writeString(primerApellido);
        dest.writeString(segundoApellido);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }
}